import React from 'react'
import injectTapEventPlugin from 'react-tap-event-plugin';

import MuiThemeProvider     from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme          from 'material-ui/styles/getMuiTheme';

import LeftDrawer           from '../components/LeftDrawer';


try { injectTapEventPlugin(); } catch (e) {  }

const muiTheme = getMuiTheme({ userAgent: false });

export default class extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            page_title: 'aboutaaaaa'
        }
    }

    render() {

        const page_title = this.state.page_title;

        return (
            <MuiThemeProvider muiTheme={muiTheme}>

                <div>
                    <style global jsx>{
                        `body {
                        background: black;
                        }`}</style>
                    <LeftDrawer page_title={page_title}/>
                    <h2>About</h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec luctus erat at tristique iaculis. Curabitur vitae erat finibus, sodales nulla vitae, consequat mauris. Fusce fermentum venenatis sem, ac maximus lacus rutrum eget. Cras fermentum mollis odio sit amet iaculis. Phasellus condimentum faucibus lorem sed rutrum. Donec pulvinar et tellus id venenatis. Nullam vel odio vitae massa condimentum ullamcorper at vitae enim. Aenean facilisis arcu nec felis tincidunt, ornare rutrum lectus tincidunt. Sed purus urna, dapibus eu auctor a, lacinia eget mauris. Donec magna diam, egestas varius facilisis vel, commodo in leo.
                    </p>
                </div>
            </MuiThemeProvider>

        )
    }
}
