// import '../stylesheets/test.css';

import React                from 'react';
import Head                 from 'next/head';
import injectTapEventPlugin from 'react-tap-event-plugin';

import MuiThemeProvider     from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme          from 'material-ui/styles/getMuiTheme';


import LeftDrawer           from '../components/LeftDrawer';

try { injectTapEventPlugin(); } catch (e) {  }

const muiTheme = getMuiTheme({
    palette: {
        textColor: 'white',
    },
    appBar: {
        height: 50,
    },
});



export default class extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            page_title: 'top'
        }
    }

    render() {
        const page_title = this.state.page_title;

        return (
            <MuiThemeProvider muiTheme={muiTheme}>

                <div>
                    <style global jsx>{
                        `body {
                        background: black;
                        }`}</style>
                    <Head>
                        <title>Next.js 2 with Material-UI Example</title>
                        <meta name="viewport" content="initial-scale=1.0, width=device-width"/>
                    </Head>

                    <LeftDrawer page_title={page_title} />

                    <h1>Page content</h1>
                    <p>Next.js 2 with Material-UI Example</p>

                </div>
            </MuiThemeProvider>
        )
    }
}