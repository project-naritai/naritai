import React        from 'react';

import Drawer       from 'material-ui/Drawer';
import MenuItem     from 'material-ui/MenuItem';
import AppBar       from 'material-ui/AppBar';

import MuiThemeProvider     from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme          from 'material-ui/styles/getMuiTheme';
import {cyan500,}           from 'material-ui/styles/colors';




import Link         from 'next/link';

const muiTheme = getMuiTheme({
    palette: {
        textColor: cyan500,
    },
    appBar: {
        height: 50,
    },
});


export default class LeftDrawer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
            data: []
        };
    }



    handleToggle = () => this.setState({open: !this.state.open});

    handleClose = () => this.setState({open: false});

    render() {
        const page_title = this.props.page_title;

        return (

            <div>

                <AppBar
                    style={{backgroundColor: 'black'}}
                    title={page_title}
                    iconClassNameRight="muidocs-icon-navigation-expand-more"
                    onLeftIconButtonTouchTap={this.handleToggle}
                />

                <Drawer
                    docked={false}
                    width={200}
                    style={{backgroundColor: 'black'}}
                    open={this.state.open}
                    onRequestChange={(open) => this.setState({open})}
                >



                    <MenuItem onTouchTap={this.handleClose}>
                        <Link href="/"><a>top</a></Link>
                    </MenuItem>

                    <MenuItem onTouchTap={this.handleClose}>
                        <Link href="/about"><a>about</a></Link>
                    </MenuItem>

                </Drawer>
            </div>

        );
    }
}